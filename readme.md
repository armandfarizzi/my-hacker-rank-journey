# Hacker Rank Interview Preparation Kit

list of problems : 

<ol>
    <li><a href='#problem-1'>Sock Merchant<a/></li>
    <li><a href='#problem-2'>Counting Valleys<a/></li>
    <li><a href='#problem-3'>Jumping on the Clouds<a/></li>
    <li><a href='#problem-4'>Arrays: 2D Array - DS<a/></li>
    <li><a href='#problem-5'>Arrays: Left Rotation<a/></li>
</ol>

## Problem-1

- Sock Merchant [number-1.js](./number-1.js)
  
![problem-1](./.screenshoot/problem-1.jpg)

![problem-1](./.screenshoot/problem-1a.jpg)

## Problem-2

- Counting Valleys [number-2.js](./number-2.js)

![problem-2](./.screenshoot/problem-2a.png)

![problem-2](./.screenshoot/problem-2b.png)

## Problem-3

- Jumping on the Clouds [number-3.js](./number-3.js)
  
![problem-3](./.screenshoot/problem-3a.png)

![problem-3](./.screenshoot/problem-3b.png)

## Problem-4

- Arrays: 2D Array - DS [number-1-array2d.js](./number-1-array2d.js)

![problem-4](./.screenshoot/problem-5a.png)

![problem-4](./.screenshoot/problem-5b.png)

## Problem-5

- Arrays: Left Rotation [number-2-array2d.js](./number-2-array2d.js)

![problem-5](./.screenshoot/problem-4a.png)

![problem-5](./.screenshoot/problem-4b.png)

