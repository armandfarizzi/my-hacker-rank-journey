function countingValleys(n, s) {
  let temp = s.split("");
  let height = 0;
  let count = 0;
  temp.forEach((each) => {
    if (height == -1 && each == "U") {
      count += 1;
    }
    switch (each) {
      case "U":
        height += 1;
        break;
      case "D":
        height -= 1;
        break;
    }
  });
  return count;
}

console.log(countingValleys(8, "DDUUDDUUDDUDUU")); // 3
