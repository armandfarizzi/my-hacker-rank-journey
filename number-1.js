function sockMerchant(n, ar) {
  let sorted = [...ar].sort((a, b) => a - b);
  let ans = [];
  sorted.forEach((each) => {
    if (!ans[each]) {
      ans[each] = 0;
    }

    ans[each] += 1;
  });
  let realAns = 0;
  ans.forEach((each) => {
    realAns += Math.floor(each / 2);
  });
  return realAns;
}

console.log(sockMerchant(9, [10, 20, 20, 10, 10, 30, 50, 10, 20])); // 3
