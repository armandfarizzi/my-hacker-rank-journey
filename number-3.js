function jumpingOnClouds(c) {
  let count = 0;
  for (let i = 0; i < c.length; i++) {
    if (c[i + 2] == 0 || c[i + 1] == 1) {
      i++;
    }
    count += 1;
  }
  return count - 1;
}

console.log(jumpingOnClouds([0, 0, 1, 0, 0, 1, 0, 0, 0])); // 5
